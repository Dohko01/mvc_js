
MVC.Controller = class Controller{
    constructor(props){
        //Que escuche los eventos primero antes de crear el model que es el que envia la peticion
        this.eventHandler();       
        this.model = new props.model(props.endpoint);
        this.view = new props.view(props.contentElement);
    }

    eventHandler(){
        document.body.addEventListener('onloadApp',(event) => {
            this.getData();
        });
    }

    getData(){
        this.model.getPersona()
        .then(data=>{
            this.view.notify(data);
            

        })
        .catch(console.log)
    }

  
}

MVC.controllerInstance = new MVC.Controller({
    model: MVC.Model,
    view: MVC.View,
    contentElement: document.querySelector('#contact_form'),
    endpoint: './model/persona.json'
});

document.body.dispatchEvent(new Event('onloadApp'));